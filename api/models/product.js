const Sequelize = require("sequelize")
const sequelize = new Sequelize('sql_tests', 'root', 'password', {
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});

sequelize.authenticate()
    .then(() => {
        console.log("Successfully connected to DB");
    })
    .catch((error) => {
        console.log(`Connection was not successful due to error: ${error}`);
    })

class User extends Sequelize.Model {}
User.init({
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            len: [8, 20],
            notNull: {
                msg: "Please use a password between 8-20 characters! THX"
            }
        }
    }
}, {
    sequelize,
    modelName: 'user'
});

class Token extends Sequelize.Model {}
Token.init({
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    key: {
        type: Sequelize.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    modelName: 'token'
});

class Note extends Sequelize.Model {}
Note.init({
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    subject: {
        type: Sequelize.STRING,
        allowNull: true
    },
    content: {
        type: Sequelize.TEXT('medium')
    },
    date: {
        type: Sequelize.DATEONLY
    }
}, {
    sequelize,
    modelName: 'note'
});

class Attach extends Sequelize.Model {}
Attach.init({
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    path: {
        type: Sequelize.STRING,
        allowNull: false
    }
}, {
    sequelize,
    modelName: 'attach'
});

User.hasMany(Note);
Note.belongsTo(User);

Note.hasMany(Attach);
Attach.belongsTo(Note);

User.hasOne(Token);
Token.belongsTo(User);

User.sync();
Note.sync();
Attach.sync();
Token.sync();

module.exports = {
    sequelize,
    User,
    Note,
    Attach,
    Token
};
