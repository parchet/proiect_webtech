const express = require("express");
const bodyParser = require("body-parser");
const { sequelize, User, Note, Attach, Token } = require('./models/product');
const testAPIRouter = require("./routes/testAPI");
const cors = require("cors");
const multer = require('multer');
const app = express();

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg' || file.mimetype === 'text/plain')
        cb(null, true);
    else cb(null, false);

};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 10
    },
    fileFilter: fileFilter
});
app.use(bodyParser.json());
app.use("/testAPI", testAPIRouter);
app.use(cors());



app.post('/create', async(req, res) => {
    try {
        await sequelize.sync({ force: true });
        res.status(201).json({ message: 'Table created' });
    }
    catch (err) {
        console.warn(err);
        res.status(500).json({ message: 'Server error' });
    }
});

app.get('/users', async(req, res) => {
    try {
        let users = await User.findAll();
        res.status(200).json(users);
    }
    catch (err) {
        console.warn(err);
        res.status(500).json({ message: 'Server error at retrieving users' });
    }
});

app.post('/getUserFromKey', async(req, res) => {
    let token;
    try {
        console.log(req.body);
        token = await Token.findOne({
            where: {
                key: req.body.key,
            }
        }).then(function(result) {
            console.log(result);
            User.findByPk(result.userId).then(function(result) {
                res.status(200).json(result);
            });
        });
    }
    catch (err) {
        console.warn(err);
        res.status(500).json({ message: 'Server error at retrieving users' });
    }
});


app.post('/getKeyFromUser', async(req, res) => {
    let token;
    try {
        // console.log(req.body);
        token = await Token.findOne({
            where: {
                userId: req.body.userId,
            }
        }).then(function(result) {
            // console.log(result);
            // User.findByPk(result.userId).then(function(result) {
            res.status(200).json(result);
        });
    }
    catch (err) {
        console.warn(err);
        res.status(500).json({ message: 'Server error at retrieving users' });
    }
});


app.post('/user/:uid/putKey', async(req, res) => {
    try {
        let token = req.body;
        token.userId = req.params.uid;
        await Token.create(token);
        res.status(200).json({ message: 'Token created' });
    }
    catch (e) {
        console.warn(e);
        res.status(500).json({ message: `Server error at creating token due to ${e}` });
    }
});

app.post('/user/:uid/newNote', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid);
        if (user) {
            let note = req.body;
            note.userId = user.id;
            await Note.create(note);
            res.status(201).json({ message: 'Note created' });
        }
        else {
            res.status(404).json({ message: 'not found' });
        }
    }
    catch (err) {
        console.warn(err);
    }
});

app.put("/note/:nid", async(req, res, next) => {
    try {
        await Note.update({
            title: req.body.title,
            content: req.body.content,
            date: req.body.date,
            subject: req.body.subject
        }, {
            where: {
                id: req.params.nid
            }
        }).then(function(result) {
            res.json(result);
        });
    }
    catch (err) {
        next(err);
    }
});

app.post("/note/:nid/newAttach", upload.single('attach'), async(req, res, next) => {
    try {
        let note = await Note.findByPk(req.params.nid);
        if (note) {
            let attach = req;
            // console.log(req);
            attach.noteId = req.params.nid;
            attach.path = req.file.path;
            await Attach.create(attach);
            res.status(201).json({ message: 'Attached successfuly' });
        }
        else {
            console.log(req.file.path);
            res.status(404).json({ message: 'Error on attaching' });
        }
    }
    catch (err) {
        console.warn(err);
    }
});

app.get('/getCredentialsEmail/:info1', async(req, res, next) => {
    try {
        await User.findOne({
            where: {
                email: req.params.info1,
            }
        }).then(function(JohnDoe) {
            if (JohnDoe) {
                res.status(200).json(JohnDoe);
            }
            else {
                res.json(-1);
            }
        });

    }
    catch (err) {
        next(err);
    }
});


app.get('/getId/:info1/:info2', async(req, res, next) => {
    try {
        await User.findOne({
            where: {
                email: req.params.info1,
                password: req.params.info2
            }
        }).then(function(JohnDoe) {
            if (JohnDoe) {
                res.status(200).json(JohnDoe.id);
            }
            else {
                res.json(-1);
            }
        });

    }
    catch (err) {
        next(err);
    }
});

app.get('/note/:id', async(req, res, next) => {
    try {
        let note = await Note.findByPk(req.params.id);
        if (note) {
            res.status(200).json(note);
        }
        else {
            res.status(404).json({ message: 'not found' });
        }
    }
    catch (err) {
        next(err);
    }
});

app.post('/newUser', async(req, res) => {
    try {
        let user = req.body;
        await User.create(user);
        res.status(200).json({ message: 'Created' });
    }
    catch (e) {
        console.warn(e);
        res.status(500).json({ message: `Server error at creating the user due to error: ${e}` });
    }
});

app.get('/user/:uid/notes', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid, {
            include: [Note]
        });
        if (user) {
            res.status(200).json(user.notes);
        }
        else {
            res.status(404).json({ message: 'not found' });
        }
    }
    catch (err) {
        next(err);
    }
});

app.delete('/user/:uid/notes/:nid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid);
        if (user) {
            let notes = await user.getNotes({
                where: {
                    id: req.params.nid
                }
            });
            let note = notes.shift();
            if (note) {
                await note.destroy();
                res.status(202).json({ message: 'accepted' });
            }
            else {
                res.status(404).json({ message: 'not found' });
            }
        }
        else {
            res.status(404).json({ message: 'not found' });
        }
    }
    catch (err) {
        next(err);
    }
});

app.delete('/user/:uid', async(req, res, next) => {
    try {
        let user = await User.findByPk(req.params.uid);
        if (user) {
            await user.destroy();
            res.status(202).json({ message: 'accepted' });
        }
        else {
            res.status(404).json({ message: 'not found' });
        }

    }
    catch (err) {
        next(err);
    }
});

app.listen(8081);
