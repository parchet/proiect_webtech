import React, { Component } from "react";
import axios from "axios";
import "./Notes.css";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';
import Select from 'react-select';
const breaks = require('remark-breaks');
const IP = process.env.REACT_APP_IP;

var inputs = ``;
var inputSubject = '';
var inputTitle = '';
var options = [];
const searchOptions = [
  { value: 'title', label: 'Title' },
  { value: 'subject', label: 'Subject' },
  { value: 'date', label: 'Date' },
  { value: 'keywords', label: 'Keywords' },
];
var searchOption = null;
var searchText = '';
var create = true;
var previousOption = null;
var noteID = null;
var incarcat = false;
const tempDate = new Date();
const date = tempDate.getUTCFullYear() + '-' + tempDate.getUTCMonth() + 1 + '-' + tempDate.getUTCDate();

class Notes extends Component {
  state = {
    selectedOption: null,
  };

  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  constructor(props) {
    super(props);
    this.state = { editMode: false, result: '', subject: '', title: '', search: '' };
    this.handleClick = this.handleClick.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleChangeInput1 = this.handleChangeInput1.bind(this);
    this.handleChangeInput2 = this.handleChangeInput2.bind(this);
    this.handleChangeInput3 = this.handleChangeInput3.bind(this);
    this.handleChangeInput4 = this.handleChangeInput4.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleVerficationKey = this.handleVerficationKey.bind(this);
    this.handleSearch = this.handleSearch.bind(this);

    if (options[0] === undefined) {
      const { cookies } = this.props;
      axios.get(`http://${IP}:8081/user/${cookies.get("id")}/notes`)
        .then(response => {
          for (var i = 0; i < response.data.length; i++)
            options.push({ value: response.data[i].id, label: response.data[i].title });
        })
        .catch(function(error) {
          console.log(error);
        });
    }
  }


  handleClick() {
    this.setState({ editMode: !this.state.editMode });
    const { cookies } = this.props;
    if (this.state.editMode) {
      if (create) {
        axios.post(`http://${IP}:8081/user/${cookies.get("id")}/newNote`, {
            title: this.state.title,
            subject: this.state.subject,
            content: inputs,
            date: date,
            userId: cookies.get("id"),
          })
          .then(function(response) {
            console.log(response);
            window.location = `http://${IP}:8000/Notes`;

          })
          .catch(function(error) {
            console.log(error);
          });
      }
      else if (noteID !== null) {
        axios.put(`http://${IP}:8081/note/${noteID}`, {
            title: inputTitle,
            subject: inputSubject,
            content: inputs,
          })
          .then(function(response) {
            console.log(response);
            window.location = `http://${IP}:8000/Notes`;

          })
          .catch(function(error) {
            console.log(error);
          });
      }
    }
  }

  handleDelete() {
    if (previousOption !== null) {
      const { cookies } = this.props;
      axios.delete(`http://${IP}:8081/user/${cookies.get("id")}/notes/${previousOption.value}`);
      window.location = `http://${IP}:8000/Notes`;
    }
  }

  handleChange = selectedOption => {
    axios.get(`http://${IP}:8081/note/${selectedOption.value}`)
      .then(response => {
        this.setState({ result: response.data.content, title: response.data.title, subject: response.data.subject }, () => {
          inputs = this.state.result;
          inputSubject = this.state.subject;
          inputTitle = this.state.title;
          noteID = selectedOption.value;
          create = false;

        });
        previousOption = selectedOption;
        this.props.history.push("./Social");
        this.props.history.push("./Notes");
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleChangeInput1(event) {
    this.setState({ subject: event.target.value });
    inputSubject = event.target.value;
  }

  handleChangeInput2(event) {
    this.setState({ title: event.target.value });
    inputTitle = event.target.value;
  }

  handleChangeInput3(event) {
    this.setState({ result: event.target.value });
    inputs = event.target.value;
  }

  handleChangeInput4(event) {
    this.setState({ search: event.target.value });
    searchText = event.target.value;
  }

  handleVerficationKey(event) {
    const { cookies } = this.props;
    let temp_key_user = Math.floor(Math.random() * (999999991 - 1000000)) + 1000000;
    let hasKey = false;
    axios.post(`http://${IP}:8081/getKeyFromUser`, {
        userId: cookies.get("id")
      })
      .then((response) => {
        if (response.data !== null) {
          alert(response.data.key);
          hasKey = true;
        }
        else {
          if (!hasKey) {
            axios.post(`http://${IP}:8081/user/${cookies.get("id")}/putKey`, {
                key: temp_key_user
              })
              .then((response) => {
                if (response !== null) {
                  console.log(response);
                  alert(temp_key_user);
                }
              }, (error) => {
                console.log(error);
              });
          }
        }
      }, (error) => {
        console.log(error);
      });
  }


  handleFileChange(event) {
    const file = React.createRef(null);
    file.current = event.target.files[0];
    const data = new FormData();
    data.append("attach", file.current);
    if (file.current != null && noteID) {
      axios({
        method: 'post',
        url: `http://${IP}:8081/note/${noteID}/newAttach`,
        data: data,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        }
      }).then(function(response) {
        if (response.status === 201) {
          incarcat = true;
          alert("File successfuly uploaded");
        }
        else alert("There was a problem loading your file, we're not sorry");
      });
    }
  }

  handleSearch() {
    const { cookies } = this.props;
    axios.get(`http://${IP}:8081/user/${cookies.get("id")}/notes`)
      .then(response => {
        if (searchText !== '') {
          options.length = 0;
          if (searchOption === "title")
            for (var i = 0; i < response.data.length; i++) {
              if (response.data[i].title === searchText)
                options.push({ value: response.data[i].id, label: response.data[i].title });
            }
          else if (searchOption === "subject")
            for (i = 0; i < response.data.length; i++) {
              if (response.data[i].subject === searchText)
                options.push({ value: response.data[i].id, label: response.data[i].title });
            }
          else if (searchOption === "date")
            for (i = 0; i < response.data.length; i++) {
              if (response.data[i].date === searchText)
                options.push({ value: response.data[i].id, label: response.data[i].title });
            }
          else if (searchOption === "keywords")
            for (i = 0; i < response.data.length; i++) {
              if (response.data[i].content.includes(searchText))
                options.push({ value: response.data[i].id, label: response.data[i].title });
            }
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    const ReactMarkdown = require('react-markdown/with-html');
    const { selectedOption } = this.state;
    const { cookies } = this.props;
    return (

      <div className="noteContainer">
      {create ? !this.state.editMode ? 
        <Select
        value={selectedOption}
        onChange={selectedOption => {searchOption = selectedOption.value}}
        options={searchOptions}
        placeholder="Search notes by:"
      />:null : null}
      
      {create ? !this.state.editMode ?
      <FormControl
            autoFocus
            type="subject"
            value={searchText}
            onChange={e => this.handleChangeInput4(e)}
          />:null : null}
          
      {create ? !this.state.editMode ?
      <Button className="actionBtn" onClick={this.handleSearch}>
      Search
      </Button>:null : null}
      
      {create ? !this.state.editMode ? 
      <Select
      value={selectedOption}
      onChange={this.handleChange}
      options={options}
      placeholder="Notes"
      />:null : null}
      
      <ReactMarkdown source={inputs} className = {this.state.editMode ? 'hidden' : 'shown'} plugins={[breaks]}/>
      {!this.state.editMode ? null:
      <div>
      <ControlLabel>Subject</ControlLabel>
      <FormControl
            autoFocus
            type="subject"
            value={inputSubject}
            onChange={e => this.handleChangeInput1(e)}
          />
      <ControlLabel>Title</ControlLabel>
      <FormControl
            autoFocus
            type="subject"
            value={inputTitle}
            onChange={e => this.handleChangeInput2(e)}
          />
      <br></br>
      </div>
      }
      
      {this.state.editMode ? 
      <textarea className = "shown" value={inputs} onChange={this.handleChangeInput3}/>:null}
      {cookies.get("verif_key") === undefined ? 
        <Button className="actionBtn" onClick={this.handleClick}>
        {create ? 'Create' : !this.state.editMode ? 'Edit' : 'Save'}
        </Button>:null}

        {(create && this.state.editMode) || (!create && !this.state.editMode) ?
        <Button className="actionBtn" onClick ={()=>{window.location = `http://${IP}:8000/Notes`}}>
        Cancel
        </Button>:null}
        {this.state.editMode && !create ?
        <Button className="actionBtn" onClick={this.handleDelete}>
        Delete
        </Button>:null
        }
        {cookies.get("verif_key") === undefined && create ? !this.state.editMode ?
        <Button className="shareKeyBtn" onClick={this.handleVerficationKey}>
        {'Get your sharing key'}
        </Button>:null : null}
        
        {!this.state.editMode ? null:
        <FormGroup onChange={this.handleFileChange} controlId="file">
        <ControlLabel>Attachment</ControlLabel>
        <FormControl type="file" />
        </FormGroup>
        }
        {!this.state.editMode ? null:
        <div text={incarcat ? 'Incarcat' : 'Waiting a file...'}>
        </div>
        }
      </div>
    );
  }
}

export default withCookies(Notes);
