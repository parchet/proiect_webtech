import React from "react";
import { Link } from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "./Main.css";
import Routes from "./Routes";
import { withCookies, Cookies } from 'react-cookie';
import { instanceOf } from 'prop-types';
import axios from "axios";

var logged = 0;

class Main extends React.Component {
  static propTypes = {
    cookies: instanceOf(Cookies).isRequired
  };

  constructor(props) {
    super(props);
    const { cookies } = props;
    if (cookies.get('email') !== undefined && cookies.get('password') !== undefined) {
      logged = 1;
      const email = cookies.get('email');
      const password = cookies.get('password');
      axios.get(`http://${process.env.REACT_APP_IP}:8081/getId/${email}/${password}`)
        .then(response => {
          cookies.set("id", response.data);
        });
    }
  }

  render() {
    return (
      <div className="App container">
      <Navbar fluid collapseOnSelect>
      <Navbar.Header>
      <Navbar.Brand>
      <Link to="/">NotesApp</Link>
      </Navbar.Brand>
      
      <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
      <Nav pullRight>
      
      {logged === 1 ?
      <LinkContainer to="/notes">
      <NavItem>Notes</NavItem>
      </LinkContainer>: null}
      
      {logged === 1 ?
      <LinkContainer to="/social">
      <NavItem>Social</NavItem>
      </LinkContainer>: null}
      
      {logged === 1 ? <NavItem onClick = {()=>{
      const { cookies } = this.props;
      logged = 0;
      cookies.remove('email');
      cookies.remove('password');
      cookies.remove('verif_key');
      window.location = `http://${process.env.REACT_APP_IP}:8000`;
      }}
      >Log Out</NavItem> : null}
      
      {logged === 0 ?
      <LinkContainer to="/login">
      <NavItem>Login</NavItem>
      </LinkContainer>: null}
      
      {logged === 0 ?
      <LinkContainer to="/signup">
      <NavItem>Signup</NavItem>
      </LinkContainer>: null}
      
      {logged === 0 ?
      <LinkContainer to="/authorize">
      <NavItem>Shared notes</NavItem>
      </LinkContainer>: null}
      
      </Nav>
      </Navbar.Collapse>
      </Navbar>
      <Routes />
    </div>
    );
  }
}

export default withCookies(Main);
