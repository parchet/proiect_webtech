import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import NotFound from "./containers/NotFound";
import Login from "./containers/Login";
import Notes from "./Notes";
import Social from "./Social";
import Signup from "./containers/Signup";
import Authorize from "./containers/Authorization";

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/login" exact component={Login} />
      <Route path="/Notes" component={Notes}/>
      <Route path="/Social" component={Social}/>
      <Route path="/Signup" component={Signup}/>
      <Route path="/authorize" component={Authorize}/>
    <Route component={NotFound} />
    </Switch>
  );
}
