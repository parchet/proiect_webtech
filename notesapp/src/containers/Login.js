import React, { useState } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import { useCookies } from 'react-cookie';
import { GoogleLogin } from 'react-google-login';
import GoogleButton from 'react-google-button';
import axios from "axios";
const IP = process.env.REACT_APP_IP;

export default function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [cookies, setCookie] = useCookies(['email', 'password']);
  const responseGoogle = (response) => {
    console.log(response);
    registerUser(response);
  };

  function registerUser(responseMain) {
    const password = getRndInteger(12345678, 999999991);
    axios.get(`http://${IP}:8081/getCredentialsEmail/${responseMain.profileObj.email}/`)
      .then(response => {
        if (response.data === -1) {
          axios({
            method: 'post',
            url: `http://${IP}:8081/newUser`,
            data: {
              name: responseMain.profileObj.name,
              email: responseMain.profileObj.email,
              password: password
            }
          }).then((response) => {
            if (response.status === 200) {
              alert("Login successful");
              setCookie('email', responseMain.profileObj.email, { path: '/' });
              setCookie('password', password, { path: '/' });
              window.location = `http://${IP}:8000/`;
            }
          }, (error) => {
            console.log(error);
          });
        }
        else {
          axios.get(`http://${IP}:8081/getCredentialsEmail/${responseMain.profileObj.email}/`)
            .then(response => {
              alert("Login successful");
              setCookie('email', response.data.email, { path: '/' });
              setCookie('password', response.data.password, { path: '/' });
              window.location = `http://${IP}:8000/`;
            });

        }
      });
  }


  function handleSubmit(event) {
    event.preventDefault();
    axios.get(`http://${IP}:8081/getId/${email}/${password}`)
      .then(response => {
        if (response.data !== -1) {
          alert("Login successful");
          setCookie('email', email, { path: '/' });
          setCookie('password', password, { path: '/' });
          window.location = `http://${IP}:8000/`;
        }
        else alert("Invalid credentials");
      })
      .catch(function(error) {
        console.log(error);
      });

  }

  function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  function validateForm() {
    return email.length > 0 && password.length > 0;
  }

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
             <Button block bsSize="large" disabled={!validateForm()} type="submit">
          Login
        </Button>
        <GoogleLogin
    clientId="1093876611952-071lb6i82bk84361nnpgfdrrfi1si8mg.apps.googleusercontent.com"
    render={renderProps => (
    <GoogleButton
    className="GoogleButton"
    onClick={renderProps.onClick}
    type="dark"
    />
    )}
    buttonText="Login"
    onSuccess={responseGoogle}
    onFailure={responseGoogle}
    cookiePolicy={'single_host_origin'}
  />
      </form>
      
      </div>

  );
}
