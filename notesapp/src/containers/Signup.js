import React from "react";
import {
  FormGroup,
  FormControl,
  ControlLabel
} from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { useFormFields } from "../libs/hooksLib";
import "./Signup.css";
import axios from "axios";
const IP = process.env.REACT_APP_IP;

export default function Signup(props) {
  const [fields, handleFieldChange] = useFormFields({
    name:"",
    email: "",
    password: "",
    confirmPassword: "",
    confirmationCode: ""
  });

  function validateForm() {
    return (
      fields.email.length > 0 &&
      fields.password.length > 0 &&
      fields.password === fields.confirmPassword
    );
  }

  function handleSubmit(event) {
    event.preventDefault();
    axios({
        method:'post',
        url:`http://${IP}:8081/newUser`,
        data:{
            name: fields.name,
            email: fields.email,
            password : fields.password
        }
    }).then((response)=>{
      alert("Registration successful");
      window.location = `http://${IP}:8000/login`;
        // console.log(response);
    }, (error)=>{
        // console.log(error);
    });
  }

  function renderForm() {
    return (
      <form onSubmit={handleSubmit}>
      <FormGroup controlId="name" bsSize="large">
          <ControlLabel>Name</ControlLabel>
          <FormControl
            type="content"
            value={fields.name}
            onChange={handleFieldChange}
          />
        </FormGroup>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={fields.email}
            onChange={handleFieldChange}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            type="password"
            value={fields.password}
            onChange={handleFieldChange}
          />
        </FormGroup>
        <FormGroup controlId="confirmPassword" bsSize="large">
          <ControlLabel>Confirm Password</ControlLabel>
          <FormControl
            type="password"
            onChange={handleFieldChange}
            value={fields.confirmPassword}
          />
        </FormGroup>
        <LoaderButton
          block
          type="submit"
          bsSize="large"
          disabled={!validateForm()}
        >
          Signup
        </LoaderButton>
      </form>
    );
  }

  return (
    <div className="Signup">
      { renderForm() }
    </div>
  );
}