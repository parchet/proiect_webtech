import React, { useState } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import { useCookies } from 'react-cookie';
import axios from "axios";
const IP = process.env.REACT_APP_IP;

export default function Authorize(props) {
    const [verif_key, setVerif_key] = useState("");
    const [cookies, setCookie] = useCookies(['verif_key']);


    function handleSubmit(event) {
        event.preventDefault();
        axios.post(`http://${IP}:8081/getUserFromKey`, {
                key: verif_key
            })
            .then((response) => {
                if (response != null) {
                    setCookie('verif_key', verif_key, { path: '/' });
                    setCookie('email', response.data.email, { path: '/' });
                    setCookie('password', response.data.password, { path: '/' });
                    console.log(response);
                    window.location = `http://${IP}:8000/`;
                }
            }, (error) => {
                console.log(error);
            });

    }

    return (
        <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Verification code</ControlLabel>
          <FormControl
            autoFocus
            type="text"
            value={verif_key}
            onChange={e => setVerif_key(e.target.value)}
          />
          </FormGroup>
             <Button block bsSize="large" disabled={verif_key.length<8} type="submit">
          Verify
        </Button>
      </form>
      </div>

    );
}
