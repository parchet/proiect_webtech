import React, { Component } from "react";
 
class Social extends Component {
  render() {
    return (
      <div>
        <h2>GOT QUESTIONS?</h2>
        <p>The easiest thing to do is post on
        our <a href="https://www.emag.ro">forums</a>.
        </p>
      </div>
    );
  }
}
 
export default Social;