import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";
import { BrowserRouter as Router } from 'react-router-dom';
import "./index.css";
import { CookiesProvider } from 'react-cookie';
ReactDOM.render(
<CookiesProvider>
  <Router>
    <Main />
  </Router>
</CookiesProvider>,
  document.getElementById('root')
);