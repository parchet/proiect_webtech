import React, { Component, useRef, useState } from "react";
import Note from "./Note";
import axios from "axios";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "./components/LoaderButton";

const breaks = require('remark-breaks');
var inputs = `# Hello,
this
is
a
sentence.`;

class Notes extends Component{
    constructor(props) {
    super(props);
    this.state = {editMode: false, result : null, clientIp:null};
    this.handleClick = this.handleClick.bind(this);
    this.handleLoad = this.handleLoad.bind(this);
    this.childRef = React.createRef();
}
  
   componentDidMount() {
        if(this.childRef.current != null){
        this.childRef.current.noteRef.current.hidden = true;
        this.childRef.current.state.html = inputs;
        }
    }
    
    handleClick() {
      this.setState({editMode: !this.state.editMode});
      if(this.childRef.current != null){
          if(this.state.editMode){
            inputs = this.childRef.current.noteRef.current.innerText;
            console.log(this.childRef.current);
          }
    }
    if(this.childRef.current != null){
        if(!this.state.editMode){
          if(this.state.result)
            this.childRef.current.noteRef.current.textContent = this.state.result;
        }
        this.childRef.current.noteRef.current.hidden = !this.childRef.current.noteRef.current.hidden;
    }
}

    handleLoad(){
      axios.get('http://3.19.69.134:8081/user/2/notes')
      .then(response => {
        this.setState({ result: response.data[0].content }, () => {inputs = this.state.result});
      })
      
      .catch(function (error) {
        console.log(error);
      });
        console.log(this.childRef.current);
    }
       
  render() {
      const ReactMarkdown = require('react-markdown/with-html');
    return (
      <div>
      <ReactMarkdown source={inputs} className = {this.state.editMode ? 'hidden' : 'shown'} plugins={[breaks]}/>
      <Note ref={this.childRef}/>
        <button onClick={this.handleClick}>
        {!this.state.editMode ? 'Edit' : 'Save'}
        </button>
        <button onClick={this.handleLoad}>
        Load
        </button>
      </div>
    );
  }
}

export default Notes;