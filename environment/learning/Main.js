import React, { Component } from "react";
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./Home";
import Notes from "./Notes";
import Social from "./Social";

class Main extends Component {
  render() {
    return (
      <HashRouter>
        <div>
          <h1>Pagina.com</h1>
          <ul className="header">
            <li><NavLink exact to="/">Home</NavLink></li>
            <li><NavLink to="/Notes">Notes</NavLink></li>
            <li><NavLink to="/Social">Social Stuff</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route path="/Notes" component={Notes}/>
            <Route path="/Social" component={Social}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default Main;