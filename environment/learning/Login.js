import React, { useState, useEffect, Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import { useCookies } from 'react-cookie';
import { GoogleLogin } from 'react-google-login';
import axios from "axios";
const IP=process.env.REACT_APP_IP;

export default function Login(props) {
const [email, setEmail] = useState("");
const [password, setPassword] = useState("");
const [cookies, setCookie] = useCookies(['email', 'password']);

  function handleSubmit(event) {
    event.preventDefault();
    axios.get(`http://${IP}:8081/getId/${email}/${password}`)
        .then(response => {
      if(response.data!==-1){
      alert("Login successful");
      setCookie('email', email, { path: '/' });
      setCookie('password', password, { path: '/' });
      window.location = `http://${process.env.REACT_APP_IP}:8000/`;
    }
       else alert("Invalid credentials");
      })
      .catch(function (error) {
        console.log(error);
      });

  }
  
  function validateForm() {
    return email.length > 0 && password.length > 0;
  }
  
  function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();
  console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
  console.log('Name: ' + profile.getName());
  console.log('Image URL: ' + profile.getImageUrl());
  console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}
  
  
    return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="email" bsSize="large">
          <ControlLabel>Email</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={email}
            onChange={e => setEmail(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
             <Button block bsSize="large" disabled={!validateForm()} type="submit">
          Login
        </Button>
      </form>
      </div>
    
  );
}
